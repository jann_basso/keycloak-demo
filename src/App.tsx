import { FC } from 'react'
import NavDrawer from './components/NavDrawer'
//import NavBar from './components/NavBar'
import { Routes, Route } from 'react-router-dom'
import routes from './utils/routes'
import { MenuContextProvider } from './utils/MenuContext'
import PagesLayout from './components/PagesLayout'

const App: FC = () => {
  return (
    <div className="App">
      <MenuContextProvider>
        <NavDrawer />
        {/* <NavBar /> */}
        <PagesLayout>
          <Routes>
            {routes.map((route, index) => {
              return <Route key={index} {...route} />
            })}
          </Routes>
        </PagesLayout>
      </MenuContextProvider>
    </div>
  )
}

export default App
