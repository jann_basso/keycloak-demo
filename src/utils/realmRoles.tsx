// const created only for demonstration purpuses
// in this project, used only once in page PaymentRequest
export const realmRoles = [
  { name: 'Admin', authView: true, authInteract: true },
  { name: 'Sales Partner', authView: true, authInteract: true },
  { name: 'Customer', authView: true, authInteract: true },
  { name: 'Customer View', authView: true, authInteract: false },
]

export const withInteractAuth = (): {
  name: string
  authView: boolean
  authInteract: boolean
}[] => {
  return realmRoles.filter((role) => role.authInteract === true)
}
