import RolesProtectedRoute from '../components/RolesProtectedRoute'
import Product from '../pages/Product'
import PaymentRequest from '../pages/PaymentRequest'
import Home from '../pages/Home'
import MyToken from '../pages/MyToken'

const routes = [
  {
    path: '/',
    element: (
      <RolesProtectedRoute
        roles={['Admin', 'Sales Partner', 'Customer', 'Customer (view)']}
        page="Home"
      >
        <Home />
      </RolesProtectedRoute>
    ),
  },
  {
    path: '/product',
    element: (
      <RolesProtectedRoute roles={['Admin', 'Sales Partner']} page="Product">
        <Product />
      </RolesProtectedRoute>
    ),
  },
  {
    path: '/payment-request',
    element: (
      <RolesProtectedRoute
        roles={['Admin', 'Sales Partner', 'Customer', 'Customer (view)']}
        page="Payment Request"
      >
        <PaymentRequest />
      </RolesProtectedRoute>
    ),
  },
  {
    path: '/my-token',
    element: <MyToken />,
  },
]

export default routes
