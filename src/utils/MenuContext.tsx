import { createContext, useState } from 'react'

type menuOpenProps = {
  menuOpen: boolean
  toggleMenu: (menuOpen: boolean) => void
}

export const MenuContext = createContext<menuOpenProps>({
  menuOpen: false,
  toggleMenu: (menuOpen: boolean) => {
    console.log(menuOpen)
  },
})

type menuContextProviderProps = {
  children: React.ReactNode
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const MenuContextProvider = ({ children }: menuContextProviderProps) => {
  const [open, setOpen] = useState(false)

  const handleToggleMenu = (): void => {
    return setOpen(!open)
  }

  return (
    <MenuContext.Provider value={{ menuOpen: open, toggleMenu: handleToggleMenu }}>
      {children}
    </MenuContext.Provider>
  )
}
