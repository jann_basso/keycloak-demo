import { styled } from '@mui/material/styles'

export const drawerWidth = 240

export const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}))

export const menuPageItems = [
  {
    text: 'Home',
    navigate: '/',
  },
  {
    text: 'Product',
    navigate: '/product',
  },
  {
    text: 'Payment Request',
    navigate: '/payment-request',
  },
]

export const menuProfileItems = [
  {
    text: 'My Token',
    navigate: '/my-token',
  },
  {
    text: 'Logout',
    navigate: '',
  },
]
