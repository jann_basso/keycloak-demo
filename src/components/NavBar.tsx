// file only used for Confluence article
// in this demo we are using NavDrawer (along with DrawerHelper and MenuContext)
import { FC } from 'react'
import AuthService from '../services/AuthService'
import { Link } from 'react-router-dom'

const NavBar: FC = () => {
  return (
    <>
      <nav>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/product">Product</Link>
            </li>
            <li>
              <Link to="/product">Product</Link>
            </li>
            <li>
              <Link to="/payment-request">Payment Request</Link>
            </li>
            <li>
              <Link to="/my-token">My Token</Link>
            </li>
            {!!AuthService.isLoggedIn() && (
              <button type="button" onClick={() => AuthService.logout()}>
                Logout
              </button>
            )}
          </ul>
        </div>
      </nav>
    </>
  )
}

export default NavBar
