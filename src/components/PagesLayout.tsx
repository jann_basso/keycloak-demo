import { useContext } from 'react'
import { styled } from '@mui/material/styles'
import { MenuContext } from '../utils/MenuContext'
import { drawerWidth, DrawerHeader } from '../helpers/DrawerHelper'

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })<{
  open?: boolean
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create('margin', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `0px`,
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
  }),
}))

type PagesLayoutProps = {
  children: React.ReactNode
}

const PagesLayout = ({ children }: PagesLayoutProps): JSX.Element => {
  const { menuOpen } = useContext(MenuContext)

  return (
    <Main open={menuOpen}>
      <DrawerHeader />
      {children}
    </Main>
  )
}

export default PagesLayout
