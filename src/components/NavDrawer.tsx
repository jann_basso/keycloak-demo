import { FC, useContext } from 'react'
import { styled, useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import CssBaseline from '@mui/material/CssBaseline'
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'

import AuthService from '../services/AuthService'
import { useNavigate, useLocation } from 'react-router-dom'

import { MenuContext } from '../utils/MenuContext'
import { drawerWidth, DrawerHeader, menuPageItems, menuProfileItems } from '../helpers/DrawerHelper'
interface AppBarProps extends MuiAppBarProps {
  open?: boolean
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}))

const NavDrawer: FC = () => {
  const { menuOpen, toggleMenu } = useContext(MenuContext)
  const { pathname } = useLocation()

  const navigateTo = useNavigate()
  const theme = useTheme()

  const toggleDrawer = (): void => {
    toggleMenu(!menuOpen)
  }

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={menuOpen}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer}
            edge="start"
            sx={{ mr: 2, ...(menuOpen && { display: 'none' }) }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Keycloak Demo
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={menuOpen}
      >
        <DrawerHeader>
          <IconButton onClick={toggleDrawer}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List>
          {menuPageItems.map((item) => {
            const { text, navigate } = item
            return (
              <ListItem selected={navigate === pathname} button key={text}>
                <ListItemText primary={text} onClick={() => navigateTo(navigate)} />
              </ListItem>
            )
          })}
        </List>
        <Divider />
        <List>
          <ListItem>
            <ListItemText secondary={`${AuthService.getUsername()}`} />
          </ListItem>
          {menuProfileItems.map((item) => {
            const { text, navigate } = item
            return (
              <ListItem selected={navigate === pathname} button key={text}>
                <ListItemText
                  primary={text}
                  onClick={
                    text === 'Logout' ? () => AuthService.logout() : () => navigateTo(navigate)
                  }
                />
              </ListItem>
            )
          })}
        </List>
      </Drawer>
    </Box>
  )
}

export default NavDrawer
