import { FC, ReactElement } from 'react'
import { Navigate } from 'react-router-dom'
import AuthService from '../services/AuthService'

type RolesProtectedRouteProps = {
  roles: string[]
  page: string
  children: ReactElement | null
}

const RolesProtectedRoute: FC<RolesProtectedRouteProps> = ({ roles, page, children }) => {
  if (!AuthService.isLoggedIn()) {
    return <Navigate to="/" />
  }

  if (!AuthService.hasRole(roles)) {
    return (
      <div>
        <h1>{page} - Access denied</h1>
        <p>You can see the {page} page only if you have one of the following roles:</p>
        <ul>
          {roles.map((role, i) => (
            <li key={i}>{role}</li>
          ))}
        </ul>
      </div>
    )
  }
  return children
}

export default RolesProtectedRoute
