import { FC } from 'react'

const Product: FC = () => {
  return (
    <section>
      <h1>Product</h1>
      <p>This page is visible to users who have Admin or Sales Partner roles.</p>
    </section>
  )
}

export default Product
