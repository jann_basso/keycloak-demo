import { FC } from 'react'
import AuthService from '../services/AuthService'

const MyToken: FC = () => {
  const token = JSON.stringify(AuthService.getTokenParsed(), null, 3)
  return (
    <section>
      <h1>My Token</h1>
      <pre>{token}</pre>
    </section>
  )
}

export default MyToken
