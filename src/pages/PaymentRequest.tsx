import { FC, useState } from 'react'
import { withInteractAuth } from '../utils/realmRoles'
import AuthService from '../services/AuthService'

const PaymentRequest: FC = () => {
  const [showInfo, setShowInfo] = useState(false)

  const accessInfo = (): void => {
    const realmInteractAccess = withInteractAuth()
    const userRoles = AuthService.getUserRoles()
    const userHasInteractAccess = realmInteractAccess.some((realmRole) =>
      userRoles?.includes(realmRole.name)
    )

    return userHasInteractAccess
      ? setShowInfo(!showInfo)
      : alert('Sorry, you cannot access this information.')
  }

  return (
    <section>
      <h1>Payment Request</h1>
      <p>This page is visible to all roles (Admin, Sales Partner, Customer, Customer View)</p>
      <p>Users with the role Customers View will not be able to access the info on this button:</p>
      <button onClick={accessInfo}>{!showInfo ? 'Access Info' : 'Hide Info'}</button>
      {showInfo && <p>Great, you can see this info!</p>}
    </section>
  )
}

export default PaymentRequest
