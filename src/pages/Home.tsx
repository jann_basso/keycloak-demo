import { FC } from 'react'

const Home: FC = () => {
  return (
    <section>
      <h1>Home</h1>
      <p>This page is visible to all roles (Admin, Sales Partner, Customer, Customer View)</p>
    </section>
  )
}

export default Home
