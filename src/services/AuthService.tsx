import Keycloak from 'keycloak-js'

const keycloak = Keycloak('./keycloak.json')

const initKeycloak = (onAuthenticatedCallback: () => void): void => {
  keycloak
    .init({
      onLoad: 'login-required',
      /* onLoad: 'check-sso',
      silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.xhtml', */
      pkceMethod: 'S256',
    })
    .then((authenticated) => {
      if (!authenticated) {
        console.log('User is not authenticated')
      }
      //console.log(getToken())
      onAuthenticatedCallback()
    })
    .catch(console.error)
}

const login = keycloak.login

const logout = keycloak.logout

const getToken = (): string | undefined => keycloak.token

const isLoggedIn = (): boolean => !!keycloak.token

const getTokenParsed = () => keycloak.tokenParsed

const getUserRoles = (): string[] | undefined => getTokenParsed()?.realm_access?.roles

const getUsername = (): string => keycloak.tokenParsed?.preferred_username

const hasRole = (roles: string[]): boolean => roles.some((role) => keycloak.hasRealmRole(role))

const AuthService = {
  initKeycloak,
  login,
  logout,
  isLoggedIn,
  getToken,
  getTokenParsed,
  getUserRoles,
  getUsername,
  hasRole,
}

export default AuthService
